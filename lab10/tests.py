from django.test import TestCase, Client
from django.urls import reverse

# Create your tests here.
class Story10UnitTest(TestCase):

    def test_story10_page_exists(self):
        response = Client().get('/story10/')
        self.assertEqual(response.status_code, 200)

    def test_story10_use_template(self):
        response = Client().get('/story10/')
        self.assertTemplateUsed(response, 'story10.html')