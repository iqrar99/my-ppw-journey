![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Nginx_logo.svg/1200px-Nginx_logo.svg.png)

# Tutorial Story 10 PPW dengan menggunakan NGINX (Windows)
Berikut ini kita akan memanfaatkan NGINX sebagai Web Server kita. Apa yang perlu dilakukan untuk menggunakan NGINX dalam Story 10 PPW?

**DAFTAR ISI:**
* [INSTALASI NGINX](#instalasi-nginx)
* [MENGGUNAKAN NGINX](#menggunakan-nginx)
* [MEMULAI STORY 10](#memulai-story-10)
* [MEMASTIKAN PORT NGINX SAMA DENGAN PORT PADA DJANGO SERVER](#memastikan-port-nginx-sama-dengan-port-pada-django-server)

## INSTALASI NGINX
1. Silakan download NGINX _stabel version_ nya di [**sini**](http://nginx.org/en/download.html) (Windows)
2. Setelah didownload, kita ekstrak folder `nginx-1.16.1` lalu tempatkan setara dengan folder project kita. Seperti gambar berikut

![alt tex](https://images2.imagebam.com/5d/95/8a/a056861326530132.png)

---

## MENGGUNAKAN NGINX
1. Untuk mengaktifkan NGINX, buka folder`nginx-1.16.1` lalu kita jalankan _Command Prompt_ di sana. Setelah itu ketikkan `start nginx`.

![alt text](https://images2.imagebam.com/bc/34/06/18fff41326530724.png)

2. Setelah dijalankan silakan buka browser, lalu ketikkan `localhost` di search bar. Maka seharusnya tampilannya akan seperti ini.

![alt text](https://images2.imagebam.com/e3/53/d5/1c12fe1326530729.png)

3. untuk memastikan atau mengetahui apakah nginx kita sudah jalan. Maka ketikkan `tasklist /fi "imagename eq nginx.exe"` pada _command prompt_ nya

4. untuk mematikan nginx nya, kita ketik `nginx -s stop`. (Untuk beberapa kasus terkadang nginx nya tidak mau di stop alias masih jalan. Maka kita matikan manual melalui **Task Manager**)
--- 

## MEMULAI STORY 10
Pada sesi ini saya membuat project baru yang bertujuan untuk menguji apakah NGINX kita bekerja dengan baik atau tidak. Untuk langkah-langkah dalam membuat project dan app tentu teman-teman semua sudah pada bisa. Jadi kita langsung saja ke bagian pentingnya.

1. Di dalam folder project kita nyalakan server django. Misalnya melalui `port 8000` dan `port 9000`.
* Pertama kita buka 2 buah _command prompt_ dan menjalankan 2 server sekaligus. `python manage.py runserver 8000` dan `python manage.py runserver 9000`
* nyalakan nginx yang berada di luar folder project kita dengan menggunakan _command prompt_ dan ketikkan `start nginx`
* Buka browser dan ketikkan `localhost`. Maka kita mendapati bahwa halaman tersebut tidak berubah sama sekali. ini artinya NGINX belum menyesuaikan dengan server kita.


2. Oleh karena itu kita perlu mengubah isi file `nginx.conf` agar bisa menjalankan project kita. Caranya adalah:
  * Buka folder `nginx-1.16.1` yang kita instal tadi, lalu buka folder **conf**.
  * Backup file `nginx.conf`, karena kita akan mengubah isi dari file tersebut. Untuk jaga-jaga makanya kita perlu backup filenya.
  * Setelah kita backup filenya, buka file `ngingx.conf` yang ada di folder conf tadi menggunakan text editor (file yang dibuka bukan file hasil backup tadi). Maka kita akan melihat tampilan isi dari filenya seperti berikut.

![alt text](https://images2.imagebam.com/40/bf/3f/eac9131326551341.png)

  * Hapus semua isi dari file tersebut lalu ganti dengan kodingan sebagai berikut.

```CSS
events {
    worker_connections  1024;
}

http {
  upstream localhost {
    server localhost:8000;
    server localhost:9000;
    
    # di sini kita bisa menambahkan port-port yang lain
    # kita juga bisa menambahkan weight pada sebuah server, misalnya
    
    # server localhost:8000 weight = 3;
  }

  server {
    listen 80;
    location / {
      proxy_pass http://localhost;
    }
  }
}
```

* Setelah itu tinggal di save
 
3. Buka kembali _command prompt_ yang menjalankan NGINGX tadi, lalu ketikkan `nginx -s reload`. lalu enter. Ini bertujuan agar NGINX memuat ulang isi dari file `nginx.conf` tadi.
4. Reload `localhost` tadi. Jika halaman sudah berubah sesuai dengan halaman yang dijalankan di server django, maka NGINX kita sudah bekerja dengan baik.

## MEMASTIKAN PORT NGINX SAMA DENGAN PORT PADA DJANGO SERVER
Kita bisa memanfaatkan **views** yang ada di dalam app project kita untuk mengetahui host dan port yang sedang dijalankan oleh NGINX. Caranya sebagai berikut:
1. Di dalam views kita dapat mengetahui host dan port yang sedang dijalnkan dengan menggunakan fungsi `get_host()` dan `get_port()`
2. Misalnya kita ingin menampilkan host dan port tersebut di halaman awal kita. Maka kodingan sederhananya sebagai berikut:

```python
from django.shortcuts import render

def index(request):

    host = request.get_host()
    port = request.get_port()

    context = {
        'host':host,
        'port':port,
    }

    return render(request, 'index.html', context)
```

3. Lalu di dalam file html di bagian \<body\>, kita tinggal memasukkan host dan port tadi.

```html
<body>
    <h1>Hello World!</h1>
    <p>
        This website is served by:  <br>
        HOST: {{ host }}            <br>
        PORT: {{ port }}            
    </p>
</body>
```

4. Sekarang kita tinggal refresh berulang-ulang di halaman tersebut. Maka kita akan mendapati bahwa portnya berubah-ubah. Ini artinya ketika salah satu server sedang sibuk atau bermasalah, maka dia akan beralih ke server yang lain.

![alt text](https://images2.imagebam.com/f7/43/1e/7772831326557649.png)
![alt text](https://images2.imagebam.com/9a/f9/cf/b184541326557651.png)

5. Jika kita mencoba untuk mematikan server django `port 8000`, maka nanti ketika kita refresh localhostnya, maka port akan selalu menunjukkan `port 9000`. ini karena server pada `port 8000` sudah mati.

---

Dengan begitu maka NGINX kita sudah bekerja dan Story 10 pun sudah jadi! yay :smile:
Pada story 10 ini kita tidak harus mendeploy NGINX nya ke website kita karena pengaturannya yang rumit. Oleh karena itu kita hanya perlu mencoba melalui local server saja.

referensi :
- https://ppw-stories-iqrar.herokuapp.com/story10/
- https://www.nginx.com/resources/wiki/start/topics/examples/loadbalanceexample/
- http://nginx.org/en/docs/windows.html