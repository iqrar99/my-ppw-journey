from django.apps import AppConfig  # pragma: no cover


class Story8Config(AppConfig):  # pragma: no cover
    name = 'story8'
