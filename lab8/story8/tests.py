from django.test import TestCase, Client            # pragma: no cover
from django.urls import resolve, reverse            # pragma: no cover
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time


from .views import story8


class Story8UnitTest(TestCase):

    def test_story8_url(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_story8_using_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')

    def test_story8_using_home_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, story8)


class FunctionalTestStory8(LiveServerTestCase):

    def setUp(self):
        super(FunctionalTestStory8, self).setUp()
        opt = Options()
        opt.add_argument('--dns-prefetch-disable')
        opt.add_argument('--no-sandbox')
        opt.add_argument('--headless')
        opt.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=opt)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_h1_text_is_displayed(self):
        self.selenium.get(self.live_server_url + '/story8/')

        header = self.selenium.find_element_by_id('header-title')
        self.assertEquals(header.text, 'BOOK FINDER')

    def test_search_bar_is_exist(self):
        self.selenium.get(self.live_server_url +'/story8/')

        search_bar = self.selenium.find_element_by_id('search')
        self.assertEquals(search_bar.get_attribute('placeholder'), 'Find Your Book')

    def test_search_button_is_exist(self):
        self.selenium.get(self.live_server_url + '/story8/')

        search_button = self.selenium.find_element_by_id('search-btn')
        self.assertEquals(search_button.text, 'Search')

    def test_random_books_appeared(self):
        self.selenium.get('http://127.0.0.1:8000/story8/')

        time.sleep(10);
        books = self.selenium.find_elements_by_class_name('book-card')

        self.assertTrue(len(books) > 0)

    def test_search_is_working(self):
        self.selenium.get('http://127.0.0.1:8000/story8/')

        search_bar = self.selenium.find_element_by_id('search')
        search_bar.send_keys('Game')

        self.selenium.find_element_by_tag_name('button').send_keys(Keys.RETURN)
        time.sleep(5)
        
        
        books = self.selenium.find_elements_by_class_name('book-card')
        self.assertTrue(len(books) > 0)

