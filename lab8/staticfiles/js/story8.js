$(document).ready(function(){

    // For loading animation
    $body = $("body");
    $(document).on({
        ajaxStart: function () { $body.addClass("loading"); },
        ajaxStop: function () { $body.removeClass("loading"); }
    });


    // when user opens our page for the first time
    $("#results").empty();

    // 200 random words
    var queries = ['queue', 'requirement', 'swipe', 'cause', 'absorption', 'panel', 'trust',
        'reserve', 'director', 'chimpanzee', 'TRUE', 'due', 'powder', 'sail', 'discreet', 'ferry',
        'quantity', 'aspect', 'underline', 'contribution', 'understand', 'scale', 'modernize', 'bay',
        'announcement', 'fragrant', 'right wing', 'opera', 'promise', 'traction', 'meal', 'compose',
        'recommend', 'financial', 'parameter', 'brilliance', 'acquisition', 'scrap', 'writer', 'horizon',
        'combination', 'dream', 'excuse', 'revolution', 'appeal', 'quarter', 'expansion', 'unaware',
        'trip', 'planet', 'buy', 'color', 'teach', 'smash', 'adjust', 'rob', 'urge', 'soul', 'chocolate',
        'screen', 'deficit', 'monopoly', 'norm', 'transmission', 'composer', 'source', 'yearn', 'haunt',
        'bother', 'twin', 'hold', 'vegetation', 'satellite', 'terms', 'pillow', 'arm', 'coffin', 'rebel',
        'door', 'rise', 'total', 'collection', 'reaction', 'adoption', 'established', 'poison', 'gradual',
        'element', 'prayer', 'strikebreaker', 'freeze', 'judgment', 'sweet', 'miner', 'undress', 'laser',
        'federation', 'mature', 'occasion', 'captivate', 'superintendent', 'reckless', 'sculpture', 'plant',
        'distort', 'guide', 'formulate', 'manner', 'recycle', 'weak', 'epicalyx', 'pace', 'overall', 'divorce',
        'bang', 'punish', 'network', 'knit', 'manager', 'diameter', 'waiter', 'observation', 'cheque', 'goat',
        'abolish', 'suite', 'hide', 'jurisdiction', 'acceptance', 'blast', 'computer virus', 'safe', 'voice',
        'bee', 'greet', 'cell phone', 'thick', 'mood', 'allowance', 'shell', 'sport', 'contempt', 'brain',
        'consideration', 'rotate', 'grateful', 'collar', 'float', 'genetic', 'obese', 'peanut', 'invisible',
        'story', 'dive', 'earthquake', 'neglect', 'waist', 'classroom', 'corn', 'executrix', 'oppose',
        'apple', 'extent', 'enjoy', 'brick', 'execution', 'gallery', 'dominate', 'hike', 'tiptoe', 'obligation',
        'sickness', 'advice', 'lick', 'persist', 'carpet', 'leaf', 'budget', 'suffer', 'lack', 'herd',
        'compensation', 'rain', 'offend', 'paradox', 'separation', 'wrap', 'shorts', 'leak', 'property',
        'concede', 'elbow', 'catalogue', 'jockey', 'throne', 'thinker', 'grind', 'mud', 'publish', 'twist', 'card']

    var idx = Math.floor(Math.random() * 200)
    var search = queries[idx];

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",

        success: function (data) {
            render(data);
        },

        type: "GET",
    });

    // when user clicks the search button
    $("#search-btn").click(function(){
        var search = $("#search").val();

        // avoiding no input
        if (search.length != 0){

            $("#results").empty();
    
            $.ajax({
                url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
                dataType: "json",
    
                success: function (data) {
                    render(data);
                },
    
                type: "GET",
            });
        }

    });
        
    // when user presses enter on search bar
    $('#search').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {

            var search = $("#search").val();

            // avoiding no input
            if (search.length != 0){

                $("#results").empty();
    
                $.ajax({
                    url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
                    dataType: "json",
    
                    success: function (data) {
                        render(data);
                    },
    
                    type: "GET",
                });
            }
        }
    });

    // function to load AJAX
    function render(data) {
        var results = document.getElementById("results");
        console.log(data);

        if (data.totalItems != 0) {

            for (i = 0; i < data.items.length; i++) {
                var info_link = data.items[i].volumeInfo.infoLink;

                // handling 'undefined'
                if (!data.items[i].volumeInfo.hasOwnProperty('imageLinks')) {
                    console.log('keskip')
                    continue;
                }

                var img_source = data.items[i].volumeInfo.imageLinks.smallThumbnail;
                var book_title = data.items[i].volumeInfo.title;
                var book_authors = data.items[i].volumeInfo.authors;
                var book_category = data.items[i].volumeInfo.categories;

                if (typeof book_authors === 'undefined') {
                    book_authors = "Unknown Authors";
                }

                if (typeof book_category === 'undefined') {
                    book_category = "-";
                }

                results.innerHTML += "<div class='book-card'>" +
                    "<a href='" + info_link + "' target= '_blank' >" +
                    "<img src='" + img_source + "' class='book-cover'>" +
                    " </a>" +
                    "<h5><b>" + book_title + "</b></h5>" +
                    "<p><i>" + book_authors + "</i></p>" +
                    "<p><b>Categories: </b>" + book_category + "</p>" +
                    "</div>";

                console.log(data.items[i].volumeInfo.title);
            }

        } else {
            results.innerHTML = "<h2 class='not-found'>Sorry, your book is not found :(</h2>";
        }
    }
});