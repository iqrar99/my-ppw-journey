from django.urls import path, include, reverse
from . import views

urlpatterns = [
    path('', views.story9, name='story9'),
    path('login/', views.story9_login, name='story9_login'),
    path('logout/', views.story9_logout, name='story9_logout'),
    path('register/', views.story9_register, name='story9_register')
]
