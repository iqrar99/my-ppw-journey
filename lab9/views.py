from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.sessions.models import Session


def story9(request):
    print(type(request))

    # current user
    user_now = request.user.get_full_name() if 'AnonymousUser' != str(request.user) else 'Welcome'
    
    # if user doesn't have full name
    if len(user_now) == 0:
        user_now = request.user.get_username()
        
    context = {
        'user_now': user_now,
    }

    return render(request, 'story9.html', context)


def story9_login(request):
    # prevent an account that already login accessing this page
    if 'AnonymousUser' != str(request.user):
        return redirect('story9')

    message = ''

    if request.method == 'POST':

        username_login = request.POST['username']
        password_login = request.POST['password']

        user = authenticate(request, username = username_login, password = password_login)
        # print(user)

        if user != None:
            try:
                login(request, user)
            except :
                pass

            return redirect('story9')

        else:
            message = 'Wrong username or password'
   
    context = {
        'message' : message,
    }

    return render(request, 'story9_login.html', context)


@login_required(login_url='/story9/login') # login required if user wants to access logout url
def story9_logout(request):

    if request.method == 'POST':

        # if user click the logout button
        if request.POST['logout'] == '':
            try:
                logout(request)
            except :
                pass

        return redirect('story9')

    return render(request, 'story9_logout.html')


def story9_register(request):
    # prevent an account that already login accessing this page
    if 'AnonymousUser' != str(request.user):
        return redirect('story9')

    message_username = ''
    message_password = ''

    if request.method == 'POST':

        user_name = request.POST['username']
        user_password = request.POST['password']
        user_first_name = request.POST['first_name']
        user_last_name = request.POST['last_name']

        # check wether the username has been used by another user
        if User.objects.filter(username = user_name).exists():
            message_username = 'Username has been used'

        # check wether username is valid
        if not username_is_valid(user_name):
            message_username = 'Invalid username'

        # check password length
        if len(user_password) < 8:
            message_password = 'Passwords must have a minimum length of 8'

        # valid input
        if len(message_username) == 0 and len(message_password) == 0:    
            NewUser = User.objects.create_user(
                username = user_name, 
                password = user_password,
                first_name = user_first_name,
                last_name = user_last_name
            )
            NewUser.save()

            return redirect('story9_login')        

    context = {
        'message_username' : message_username,
        'message_password' : message_password,
    }

    return render(request, 'story9_register.html', context)

# method for validating username
def username_is_valid(username):
    for c in username:
        if not (c.isalnum() or c == '_'):
            return False

    return True

