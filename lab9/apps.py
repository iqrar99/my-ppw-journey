from django.apps import AppConfig # pragma: no cover


class Story9Config(AppConfig): # pragma: no cover
    name = 'story9'
