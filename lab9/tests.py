from django.test import TestCase, Client, RequestFactory            # pragma: no cover
from django.urls import resolve, reverse                            # pragma: no cover
from django.test import LiveServerTestCase
from django.shortcuts import render, resolve_url
from django.contrib.auth.models import User, AnonymousUser
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from django.http.response import HttpResponseRedirect

import time

from .views import story9, story9_login, story9_logout, story9_register


class Story9UnitTest(TestCase):

    def test_story9_url(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_login_url(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url(self):
        response = Client().get('/story9/logout/')
        self.assertIn(response.status_code, (200, 302))

    def test_register_url(self):
        response = Client().get('/story9/register/')
        self.assertIn(response.status_code, (200, 302))


    def test_story9_use_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9.html')

    def test_login_use_template(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'story9_login.html')

    def test_register_use_template(self):
        response = Client().get('/story9/register/')
        self.assertTemplateUsed(response, 'story9_register.html')


    def test_story9_using_story9_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, story9)

    def test_login_using_login_func(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, story9_login)

    def test_logout_using_logout_func(self):
        found = resolve('/story9/logout/')
        self.assertEqual(found.func, story9_logout)

    def test_register_using_register_func(self):
        found = resolve('/story9/register/')
        self.assertEqual(found.func, story9_register)


class Story9ViewsTest(TestCase):
    def setUp(self):

        # RequestFactory is an object to make a request for testing our views
        self.factory = RequestFactory()

        self.user = User.objects.create_user(
            username='betatester',
            email='tester@secret.com',
            password='test12345',
            first_name='beta',
            last_name='tester',
        )

    def test_fullname_is_empty(self):
        request = self.factory.get('/story9/')
        request.user = self.user
        request.user.first_name = ''
        request.user.last_name = ''

        response = story9(request)
        self.assertEqual(response.status_code, 200)

    def test_signed_in_account_access_login_page_will_get_redirect(self):
        request = self.factory.get('/story9/login/')
        request.user = self.user

        response = story9_login(request)
        self.assertIn(response.status_code, (200, 302))
        self.assertTrue(type(response) == HttpResponseRedirect)

    def test_login(self):
        request = self.factory.post('/story9/login/')
        request.user = AnonymousUser()
        request.POST = request.POST.copy()
        request.POST['username'] = 'betatester'
        request.POST['password'] = 'test12345'

        response = story9_login(request)
        self.assertIn(response.status_code, (200, 302))
        self.assertTrue(type(response) == HttpResponseRedirect)

    def test_login_failed(self):
        request = self.factory.post('/story9/login/')
        request.user = AnonymousUser()
        request.POST = request.POST.copy()
        request.POST['username'] = 'xxxxxxxx'
        request.POST['password'] = 'loremipsum'

        response = story9_login(request)
        self.assertIn(response.status_code, (200, 302))

    def test_logout(self):
        request = self.factory.post('/story9/login/')
        request.user = self.user
        request.POST = request.POST.copy()
        request.POST['logout'] = ''

        response = story9_logout(request)
        self.assertIn(response.status_code, (200, 302))
        self.assertTrue(type(response) == HttpResponseRedirect)

    def test_logout_canceled(self):
        request = self.factory.get('/story9/login/')
        request.user = self.user

        response = story9_logout(request)
        self.assertEqual(response.status_code, 200)

    def test_signed_in_account_access_register_page_will_get_redirect(self):
        request = self.factory.get('/story9/register/')
        request.user = self.user

        response = story9_register(request)
        self.assertIn(response.status_code, (200, 302))
        self.assertTrue(type(response) == HttpResponseRedirect)

    def test_register_success(self):
        request = self.factory.post('/story9/register')
        request.user = AnonymousUser()

        request.POST = request.POST.copy()
        request.POST['username'] = 'andromeda'
        request.POST['password'] = 'milkyway9000'
        request.POST['first_name'] = 'space'
        request.POST['last_name'] = 'x'

        response = story9_register(request)
        self.assertIn(response.status_code, (200, 302))
        self.assertTrue(type(response) == HttpResponseRedirect)

    def test_register_failed_username_already_used(self):
        request = self.factory.post('/story9/register')
        request.user = AnonymousUser()

        request.POST = request.POST.copy()
        request.POST['username'] = 'betatester'
        request.POST['password'] = 'milkyway9000'
        request.POST['first_name'] = 'space'
        request.POST['last_name'] = 'x'

        response = story9_register(request)
        self.assertEqual(response.status_code, 200)

    def test_regiter_failed_username_is_not_valid(self):
        request = self.factory.post('/story9/register')
        request.user = AnonymousUser()

        request.POST = request.POST.copy()
        request.POST['username'] = '^%$#(@**$_-)'
        request.POST['password'] = 'milkyway9000'
        request.POST['first_name'] = 'space'
        request.POST['last_name'] = 'x'

        response = story9_register(request)
        self.assertEqual(response.status_code, 200)

    def test_register_failed_password_is_too_short(self):
        request = self.factory.post('/story9/register')
        request.user = AnonymousUser()

        request.POST = request.POST.copy()
        request.POST['username'] = '^%$#(@**$_-)'
        request.POST['password'] = 'LOL'
        request.POST['first_name'] = 'space'
        request.POST['last_name'] = 'x'

        response = story9_register(request)
        self.assertEqual(response.status_code, 200)
